from datetime import datetime

DATE_FORMAT = '%Y-%m-%d %H:%M %Z'
datetime_validator = IS_LOCALIZED_DATETIME(timezone=pytz.timezone(user_timezone), format=DATE_FORMAT)
FULL_DATE_FORMAT = '%Y-%m-%d %H:%M:%S %Z'
full_datetime_validator = IS_LOCALIZED_DATETIME(timezone=pytz.timezone(user_timezone), format=FULL_DATE_FORMAT)

db.define_table('storage',
    Field('app_id'),
    Field('key'),
    Field('data', 'text'),
    )

db.define_table('checkin',
    Field('user'),
    Field('lat', 'double'),
    Field('lng', 'double'),
    Field('accuracy', 'double'),
    Field('grid_id'),
    Field('timestamp', 'datetime'),
    )

db.define_table('bboard',
    Field('app_id'),
    Field('message', 'text'),
    Field('post_date', 'datetime', default=datetime.utcnow(), requires=datetime_validator),
    )

db.define_table('local_messages',
    Field('msgid'),
    Field('userid'),
    Field('latlng'),
    Field('timestamp', 'datetime', default=datetime.utcnow()),
    Field('msg', 'text'),
    )
