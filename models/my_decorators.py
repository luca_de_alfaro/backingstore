from datetime import datetime

# Decorator used to check whether the user is logged in.
# A user needs to be both logged in in the web2py sense,
# and in the google sense.  If a user is not logged in,
# the user is redirected to a special 'facade' page.
def requires_login(fn):
    def f():
        if auth.user is None and get_user_email() is not None:
            # We need to renew the web2py session.
            logger.info("We need to renew the web2py session.")
            if current.request.ajax:
                logger.warning("We need to renew the web2py session - ajax")  
                raise HTTP(401, 'Not authorized')
            elif current.request.is_restful:
                logger.warning("We need to renew the web2py session - restful")  
                raise HTTP(403, "Not authorized")
            else:
                next = current.request.env.request_uri
                current.session.flash = current.response.flash
                return redirect(URL('default', 'user', args=['login'])
                        + '?_next=' + urllib.quote(next))            
        elif get_user_email() is None:
            # We go to the login page.
            if current.request.ajax:
                logger.warning("Not authorized - ajax")
                raise HTTP(401, 'Not authorized')
            elif current.request.is_restful:
                logger.warning("Not authorized - restful")
                raise HTTP(403, "Not authorized")
            else:
                next = current.request.env.request_uri
                current.session.flash = current.response.flash
                return redirect(URL('default', 'user', args=['login'], vars=dict(next=next)))
        else:
            return fn()
    return f


# This decorator is similar to the above, but rather than requiring login,
# simply checks whether one is logged in.
def checks_login(fn):
    def f():
        current.props = None
        if auth.user is None and get_user_email() is not None:
            # We need to renew the web2py session.
            if current.request.ajax:
                raise HTTP(401, 'Not authorized')
            elif current.request.is_restful:
                raise HTTP(403, "Not authorized")
            else:
                next = current.request.env.request_uri
                current.session.flash = current.response.flash
                return redirect(URL('default', 'user', args=['login'])
                        + '?_next=' + urllib.quote(next))            
        elif get_user_email() is None:
            # Not logged in.
            return fn()
        else:
            # Reads the user properties.  We check user_id AND email: the email is essential
            # in making sure that others can refer to this user.
            return fn()
    return f
